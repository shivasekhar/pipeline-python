from gaiasdk import sdk
import logging
import time



def MyAwesomeJob(args):
    logging.info("job started")
    time.sleep(10)
    logging.info("job finished")

def main():
    logging.basicConfig(level=logging.INFO)
    myjob = sdk.Job("Test Job", "My Awesome job", MyAwesomeJob)
    sdk.serve([myjob])
